package com.timur.service;

import com.timur.entity.EmployeeJDBCDAO;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BalanceReceiver {
    @RequestMapping(value = "/receivebalance", method = RequestMethod.POST)
    public void receiveBalance(@RequestParam(value = "id") Integer id,
                               @RequestParam(value = "balance") Integer balance) {
        //ToDo
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        EmployeeJDBCDAO employeeJDBCDAO =
                (EmployeeJDBCDAO) context.getBean("employeeJDBCDAO");
        employeeJDBCDAO.update(id, employeeJDBCDAO.findById(id).getName(), balance);
    }
}