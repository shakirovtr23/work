package com.timur.controllers;

import com.timur.entity.Employee;
import com.timur.entity.EmployeeJDBCDAO;
import com.timur.service.BalanceService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        EmployeeJDBCDAO employeeJDBCDAO =
                (EmployeeJDBCDAO) context.getBean("employeeJDBCDAO");

        List<Employee> employeeList = employeeJDBCDAO.findAll();
        model.addAttribute("employeeList", employeeList);
        return "index";
    }

    @RequestMapping(value = "/balanceservice/{id}", method = RequestMethod.GET)
    public String getBalance(@PathVariable("id") int id, Model model) throws Exception {
        BalanceService balanceService = new BalanceService();
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        EmployeeJDBCDAO employeeJDBCDAO =
                (EmployeeJDBCDAO) context.getBean("employeeJDBCDAO");

        employeeJDBCDAO.update(id, employeeJDBCDAO.findById(id).getName(), balanceService.getBalance(id));
        List<Employee> employeeList = employeeJDBCDAO.findAll();
        model.addAttribute("employeeList", employeeList);
        return "index";
    }
}