package com.timur.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

abstract class AbstractDBConnector implements DBConnector {

    @Override
    public Connection getConnection() {
        Connection dbConnection = null;

        try {
            Class.forName(getDriver()).newInstance();
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        try {
            dbConnection = DriverManager.getConnection(getUrl(),
                    getUser(),
                    getPassword());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return dbConnection;
    }

    abstract String getDriver();

    abstract String getPassword();

    abstract String getUser();

    abstract String getUrl();
}
