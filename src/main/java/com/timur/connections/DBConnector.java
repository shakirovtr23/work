package com.timur.connections;

import java.sql.Connection;

public interface DBConnector {
    Connection getConnection();
}
