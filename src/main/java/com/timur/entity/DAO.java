package com.timur.entity;

import org.springframework.dao.DuplicateKeyException;

import java.util.List;

public interface DAO {

    int create(int id, String name, int balance) throws DuplicateKeyException;

    int update(int id, String name, int balance);

    int delete(int id);

    List<Employee> findAll();

    Employee findById(int id);


}
