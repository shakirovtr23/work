package com.timur.entity;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

class EmployeeMapper implements RowMapper<Employee> {

    @Nullable
    @Override
    public Employee mapRow(ResultSet resultSet, int i) throws SQLException {

        return new Employee(resultSet.getInt("id"),
                resultSet.getString("name"),
                resultSet.getInt("balance"));
    }
}
