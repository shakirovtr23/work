package com.timur.entity;

public class Employee {

    private final Integer id;
    private final String name;
    private Integer balance;

/*    private static List<Employee> employeeList;


    public static Employee getInstance() {
        Employee employee = new Employee(1, "2", 1);

        int index = employeeList.indexOf(employee);

        if (index == -1) {
           employeeList.add(employee);
        } else {
            employee = employeeList.get(index);
        }

        return employee;
    }*/

    public Employee(int id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Employee)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        Employee employee = (Employee) obj;
        return this.getBalance() == employee.getBalance()
                && this.getName().equals(employee.getName())
                && this.getId() == employee.getId();
    }
}
