package com.timur.entity;

import com.timur.connections.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO implements DAO {

    private DBConnector dbConnector;

    public EmployeeDAO(DBConnector dbConnector) {
        if (dbConnector == null) {
            throw new IllegalArgumentException();
        }

        this.dbConnector = dbConnector;
    }

    @Override
    public int create(int id, String name, int balance) {
        if (dbConnector == null) {
            System.out.println("No connection established");
            return -1;
        }

        Statement stmt;
        PreparedStatement insertPreparedStatement;
        try (Connection connection = dbConnector.getConnection()) {
            connection.setAutoCommit(false);
            stmt = connection.createStatement();
            stmt.execute("CREATE TABLE IF NOT EXISTS EMPLOYEE(id int PRIMARY KEY, name varchar(255) NOT NULL, balance int NOT NULL)");

            if (id <= 0) {
                System.out.println("Wrong value: negative ID");
                return -1;
            }

            String insertQuery = "INSERT INTO EMPLOYEE(id, name, balance) VALUES" + "(?, ?, ?)";
            insertPreparedStatement = connection.prepareStatement(insertQuery);
            insertPreparedStatement.setInt(1, id);
            insertPreparedStatement.setString(2, name);
            insertPreparedStatement.setInt(3, balance);
            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();

            stmt.close();
            connection.commit();
            return id;

        } catch (SQLException e) {
            System.out.println("Exception Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int update(int id, String name, int balance) {

        if (id <= 0) {
            System.out.println("Wrong value: negative ID");
            return -1;
        }
        if (name == null) {
            System.out.println("Wrong value: name equals to NULL");
            return -1;
        }

        Statement stmt;
        PreparedStatement updatePreparedStatement;
        try (Connection connection = dbConnector.getConnection()) {
            connection.setAutoCommit(false);
            stmt = connection.createStatement();
            String updateQuery = "UPDATE EMPLOYEE SET name=?, balance=? WHERE id=?";
            updatePreparedStatement = connection.prepareStatement(updateQuery);
            updatePreparedStatement.setString(1, name);
            updatePreparedStatement.setInt(2, balance);
            updatePreparedStatement.setInt(3, id);
            updatePreparedStatement.executeUpdate();
            updatePreparedStatement.close();

            stmt.close();
            connection.commit();
            return id;

        } catch (SQLException e) {
            System.out.println("Exception Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int delete(int id) {
        if (dbConnector == null) {
            System.out.println("No connection established");
            return -1;
        }

        Statement stmt;
        PreparedStatement deletePreparedStatement;
        try (Connection connection = dbConnector.getConnection()) {
            if (id <= 0) {
                System.out.println("Wrong value: negative ID");
                return -1;
            }
            connection.setAutoCommit(false);
            stmt = connection.createStatement();
            String deleteQuery = "DELETE FROM EMPLOYEE WHERE id=?";
            deletePreparedStatement = connection.prepareStatement(deleteQuery);
            deletePreparedStatement.setInt(1, id);
            deletePreparedStatement.executeUpdate();
            deletePreparedStatement.close();

            stmt.close();
            connection.commit();
            return id;

        } catch (SQLException e) {
            System.out.println("Exception Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public List<Employee> findAll() {
        if (dbConnector == null) {
            System.out.println("No connection established");
            return new ArrayList<>();
        }

        Statement stmt;
        ResultSet rs;
        List<Employee> result = new ArrayList<>();

        try (Connection connection = dbConnector.getConnection()) {
            stmt = connection.createStatement();
            rs = stmt.executeQuery("SELECT * FROM EMPLOYEE");
            while (rs.next()) {
                result.add(new Employee(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getInt("balance")));
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Employee findById(int id) {
        if (dbConnector == null) {
            System.out.println("No connection established");
            return null;
        }

        PreparedStatement preparedStatement;
        ResultSet rs;
        Employee result = null;

        if (id <= 0) {
            System.out.println("Wrong value: negative ID");
            return null;
        }

        try (Connection connection = dbConnector.getConnection()) {
            String query = "SELECT * FROM EMPLOYEE WHERE id = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = new Employee(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getInt("balance"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
}
