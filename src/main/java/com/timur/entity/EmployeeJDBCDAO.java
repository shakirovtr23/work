package com.timur.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class EmployeeJDBCDAO implements DAO{

    private final JdbcTemplate jdbcTemplateObject;

    @Autowired
    public EmployeeJDBCDAO(@Qualifier("jbdcTemplate") JdbcTemplate jdbcTemplateObject) {

        this.jdbcTemplateObject = jdbcTemplateObject;
    }

    @Override
    public int create(int id, String name, int balance) {
        if (id <= 0 || name == null || jdbcTemplateObject == null) {
            return -1;
        }

        String SQL = "INSERT INTO EMPLOYEE(id, name, balance) VALUES (?, ?, ?)";
        jdbcTemplateObject.update(SQL, id, name, balance);
        return id;
    }

    @Override
    public int delete(int id) {
        if (id <= 0 || jdbcTemplateObject == null) {
            return -1;
        }
        String SQL = "DELETE FROM EMPLOYEE WHERE id = ?";
        jdbcTemplateObject.update(SQL, id);
        return id;
    }

    @Override
    public int update(int id, String name, int balance) {
        if (id <= 0 || name == null || jdbcTemplateObject == null) {
            return -1;
        }
        String SQL = "UPDATE EMPLOYEE SET name = ?, balance = ? WHERE id = ?";
        jdbcTemplateObject.update(SQL, name, balance, id);
        return id;
    }

    @Override
    public Employee findById(int id) {
        if (id <= 0 || jdbcTemplateObject == null) {
            return null;
        }

        String SQL = "SELECT * FROM EMPLOYEE WHERE id = ?";
        return jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new EmployeeMapper());
    }

    @Override
    public List<Employee> findAll() {
        if (jdbcTemplateObject == null) {
            return new ArrayList<>();
        }
        String SQL = "SELECT * FROM EMPLOYEE ORDER BY ID ASC ";
        return jdbcTemplateObject.query(SQL, new EmployeeMapper());
    }


}
