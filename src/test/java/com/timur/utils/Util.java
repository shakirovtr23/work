package com.timur.utils;

import com.timur.connections.DBConnector;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Util {

    private final DBConnector connector;

    public Util(DBConnector connector) {
        this.connector = connector;
    }

    public void initDBValues() {
        Statement stmt;
        try (Connection connection = connector.getConnection()) {
            connection.setAutoCommit(false);
            stmt = connection.createStatement();
            stmt.execute("DROP TABLE IF EXISTS EMPLOYEE");
            stmt.execute("CREATE TABLE IF NOT EXISTS EMPLOYEE(id int PRIMARY KEY, name varchar(255) NOT NULL, balance int NOT NULL)");
            stmt.execute("INSERT INTO EMPLOYEE(id, name, balance) VALUES(1, 'Ivan Ivanov', 0)");
            stmt.execute("INSERT INTO EMPLOYEE(id, name, balance) VALUES(2, 'Andrei Andreev', 0)");
            stmt.execute("INSERT INTO EMPLOYEE(id, name, balance) VALUES(3, 'Oleg Olegov', 0)");

            stmt.close();
            connection.commit();

        } catch (SQLException e) {
            System.out.println("Exception Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*    public static void initDBValuesSpring() {
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        EmployeeJDBCDAO employeeJDBCTemplate =
                (EmployeeJDBCDAO) context.getBean("employeeJDBCTemplate");

        employeeJDBCTemplate.create(1, "Ivan Spring", 0);
        employeeJDBCTemplate.create(2, "Oleg JDBC", 0);
        employeeJDBCTemplate.create(3, "Andrei Template", 0);
 }*/
}