package com.timur.connections;

public class H2DBTestConnector extends AbstractDBConnector {
    @Override
    String getDriver() {
        return "org.h2.Driver";
    }

    @Override
    String getPassword() {
        return "";
    }

    @Override
    String getUser() {
        return "sa";
    }

    @Override
    String getUrl() {
        return "jdbc:h2:~/testemployees";
    }
}
