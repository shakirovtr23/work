package com.timur.entity;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public abstract class DAOTest {

    DAO dao;

    @Before
    public abstract void setUp();

    @Test
    public void create() throws Exception {
        long sizeBefore = dao.findAll().size();
        Employee employee = new Employee(4, "Timur Shakirov", 15);
        dao.create(4, "Timur Shakirov", 15);
        assertEquals(sizeBefore + 1, dao.findAll().size());
        Employee result = dao.findById(employee.getId());
        assertEquals(employee, result);
    }

    @Test
    public void createWithNull() {
        assertEquals(-1, dao.create(4, null, 1));
    }

    @Test
    public void createWithNegativeId() {
        assertEquals(-1, dao.create(-1, "Timur", 1));
    }

    @Test
    public void update() throws Exception {
        Employee employee = new Employee(1, "Timur Shakirov", 15);
        dao.update(1, "Timur Shakirov", 15);
        Employee result = dao.findById(1);
        assertEquals(employee, result);
    }


    @Test
    public void updateWithNull() throws Exception {
        assertEquals(-1, dao.update(1, null, 100));
    }

    @Test
    public void updateWithZeroId() throws Exception {
        assertEquals(-1, dao.update(0, "Ivan Ivanov", 10));
    }

    @Test
    public void updateWithNegativeId() throws Exception {
        assertEquals(-1, dao.update(-1, "Ivan Ivanov", 10));
    }


    @Test
    public void deleteWithZeroId() throws Exception {
        assertEquals(-1, dao.delete(0));
    }

    @Test
    public void deleteWithNegativeId() throws Exception {
        assertEquals(-1, dao.delete(-1));
    }

    @Test
    public void findAll() throws Exception {
        List<Employee> testData = new ArrayList<>();
        testData.add(new Employee(1, "Ivan Ivanov", 10));
        testData.add(new Employee(2, "Andrei Andreev", 20));
        testData.add(new Employee(3, "Oleg Olegov", 30));
        List<Employee> result = dao.findAll();
        assertTrue(testData.containsAll(result));
    }

    @Test
    public void findById() {
        assertEquals(new Employee(1, "Ivan Ivanov", 10), dao.findById(1));
    }

    @Test
    public void findByIdWithZeroId() throws Exception {
        assertEquals(null, dao.findById(0));
    }

    @Test
    public void findByIdWithNegativeId() throws Exception {
        assertEquals(null, dao.findById(-1));
    }

}
