package com.timur.entity;

import com.timur.connections.H2DBTestConnector;
import com.timur.utils.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:Beans.xml")
public class EmployeeJDBCDAOTest extends DAOTest {

    @Autowired
    @Qualifier("testJbdcTemplate")
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public void setUp() {
        new Util(new H2DBTestConnector()).initDBValues();
        dao = new EmployeeJDBCDAO(jdbcTemplateObject);
    }

    @Test(expected = DuplicateKeyException.class)
    public void createWithDublicateId() {
        dao.create(8, "Timur", 1);
        assertEquals(-1, dao.create(8, "Timur", 1));
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void delete() throws Exception {
        long sizeBefore = dao.findAll().size();
        dao.delete(1);
        assertEquals(sizeBefore - 1, dao.findAll().size());
        dao.findById(1);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void findByIdNotInList() throws Exception {
        dao.findById(5);
    }

}