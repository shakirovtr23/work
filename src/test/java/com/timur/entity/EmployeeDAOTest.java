package com.timur.entity;

import com.timur.connections.H2DBTestConnector;
import com.timur.utils.Util;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmployeeDAOTest extends DAOTest {

    @Override
    public void setUp() {
        new Util(new H2DBTestConnector()).initDBValues();
        dao = new EmployeeDAO(new H2DBTestConnector());
    }

    @Test
    public void createWithDublicateId() {
        dao.create(8, "Timur", 1);
        assertEquals(-1, dao.create(8, "Timur", 1));
    }

    @Test
    public void delete() throws Exception {
        long sizeBefore = dao.findAll().size();
        dao.delete(1);
        assertEquals(sizeBefore - 1, dao.findAll().size());
        assertEquals(null, dao.findById(1));
    }

    @Test
    public void findByIdNotInList() throws Exception {
        assertEquals(null, dao.findById(5));
    }

}